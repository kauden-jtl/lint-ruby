FROM ruby:2.7-alpine

WORKDIR /usr/src/app

COPY Gemfile /usr/src/app/

RUN echo 'gem: --no-rdoc --no-ri' > /root/.gemrc \
 && apk add --no-cache --virtual build-packages build-base \
 && gem install bundler \
 && bundle install \
 && rm -rf /usr/share/ri \
 && apk del --no-cache --purge build-packages

CMD ["rubocop"]
